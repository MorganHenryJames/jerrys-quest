﻿//\===========================================================================================
//\ File: destructable.cs
//\ Author: Morgan James
//\ Date Created: 19/04/2017
//\ Brief: Allows the object to be replaced with a different object when the player collides with it
//\ at a certain speed after using his/her charge ability.
//\===========================================================================================

using UnityEngine;

public class destructable : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public GameObject destroyedVersion;//The game object that will replace the object this script is attached to.

	private GameObject player;//The game object that will be equal to the player.

	private GameObject empty;//A game object that will be equal to the permanent variables object.

	public float fSpeedNeeded = 10.0f;//The speed needed for the wall to be destroyed by charging into it.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		player = GameObject.Find("Player");//Sets the player to be equal to the game object of the Player.

		empty = GameObject.Find("Permanent Variables");//Sets the empty game object equal to the game object that contains the variables that wont be destroyed on scene change.
	}
	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.gameObject.CompareTag("Player"))//If the colliding object is the "Player".
		{
			if (player.GetComponent<Rigidbody>().velocity.magnitude > fSpeedNeeded && empty.GetComponent<variableHolder>().fChargeTimer > 0)//If the player is moving above speed needed and the charge ability is on cool down.
			{
				var instantiatedPrefab = Instantiate(destroyedVersion, transform.position, transform.rotation) as GameObject;//Creates the destroyedVersion game object in the place of the object this script is attached to.

				instantiatedPrefab.transform.localScale = transform.localScale;//Sets the scale of the destroyed version to the scale of the object this script is attached to.

				Destroy(gameObject);//Destroys the object this script is attached to.
			}
		}
	}

	#endregion

}



