﻿//\===========================================================================================
//\ File: spiderCollider.cs
//\ Author: Morgan James
//\ Date Created: 13/05/2017
//\ Brief: Attached to the spiders hit box. Destroys the spider if the player hits the hit box.
//\ If the keyDrop boolean is true then the spider will also drop a key.
//\===========================================================================================

using UnityEngine;

public class spiderCollider : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject empty;//A game object that will be equal to the permanent variables object.

	public ParticleSystem deathEffect;//The particle effect that will play on the death of the spider.

	public GameObject keyPrefab;//The game object that will be created on the death of the spider if the keyDrop boolean is set to true.

	public AudioClip deathSound;//The audio clip that will play when the spider dies.

	public AudioSource audioSource;//The audio source from which the sounds will play.

	public bool keyDrop = false;//A boolean variable that dictates whether a object is created on the death of a spider.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		empty = GameObject.Find("Permanent Variables");//Sets the empty game object equal to the game object that contains the variables that wont be destroyed on scene change.

		audioSource = GameObject.Find("Player").GetComponent<AudioSource>();//Sets the audio source to be equal to the one attached to the player as the spider will no longer be alive to make the death noises.
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			Instantiate(deathEffect, other.transform.position, other.transform.rotation);//Creates the death particle effect at the position of the player.

			audioSource.PlayOneShot(deathSound);//Plays the audio clip of the death sound.

			if (keyDrop == true)//If the keyDrop boolean is set to true execute the following code.
			{
				Instantiate(keyPrefab, other.transform.position, other.transform.rotation);//Create the game object equal to the keyPrefab at the position of the player.
			}

			empty.GetComponent<variableHolder>().SetCountText(3);//Increase the score by 3.

			Destroy(transform.parent.gameObject);//Destroys the spider.
		}
	}

	#endregion

}
