﻿//\===========================================================================================
//\ File: spiderAI.cs
//\ Author: Morgan James
//\ Date Created: 13/05/2017
//\ Brief: Attached to the spider to move it around and charge at the player if one is close enough.
//\===========================================================================================

using UnityEngine;

public class spiderAI : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject player;//A game object that will be equal to the player game object.

	private string state = "strafe";//The initial state.

	private NavMeshAgent nav;//The navigation mesh attached to the object

	public float fWanderDistance = 5.0f;//How far the AI can wander.

	public float fChaseSpeed = 50.0f;//How fast the AI charges.

	public float fWanderSpeed = 10.0f;//How fast the AI wanders.

	public Vector3 v3StartPos;//The initial position of the AI.

	public float fDetectionDistance = 15.0f;//How far the AI can see/detect.

	private Animator anim;//The animator the script will effect.

	private float fWaitTime = 0.0f;//How long the AI has to wait for.

	public float fChargeTime = 2.0f;//How long the AI charges for.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		v3StartPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);//Sets the starting position equal to the transform position of the object.

		player = GameObject.Find("Player");//Sets the game object equal to the player's game object.

		nav = GetComponent<NavMeshAgent>();//Sets the navigation agent equal to the one attached to the game object.

		anim = GetComponent<Animator>();//Sets the animator equal to the one attached to the object.
	}

	void Update()
	{
		if (state == "strafe")//If the current state is equal to strafe.
		{
			nav.speed = fWanderSpeed;//Sets the movement speed of the AI equal to the wandering speed.

			Vector3 randomPos = Random.insideUnitSphere * fWanderDistance;//Creates a vector equal to a position within a circle around the AI with the radius of wander distance.

			NavMeshHit navHit;//Creates a position of the navigation mesh to go to. 

			NavMesh.SamplePosition(transform.position + randomPos, out navHit, 20f, NavMesh.AllAreas);//Sets the sample position of the navigation mesh.

			float fDistance = Vector3.Distance(transform.position, player.transform.position);//Sets the fDistance to be equal to the distance between the AI and the player.

			if (fDistance < fDetectionDistance)//If the distance to the player is smaller than the detection distance execute the following code.
			{
				state = "chase";//Set the current case to be equal to the chase case.
			}

			nav.SetDestination(navHit.position);//Sets the destination of the AI.
		}

		if (state == "chase")//If in the chase state.
		{
			nav.speed = fWanderSpeed;//Sets the speed equal to the wander speed.

			nav.destination = player.transform.position;//Sets the destination of the AI to be equal to the players position.

			fWaitTime -= Time.deltaTime;//Decreases the wait time.

			fChargeTime -= Time.deltaTime;//Decreases the charge time.

			if (fWaitTime <= 0)//If the wait time is less than or equal to zero execute the following code.
			{
				if (fChargeTime >= 0)//If the charge timer is more than or equal to zero execute the following code.
				{
					anim.speed = 5.0f;//Sets the speed of the animation to be 5.

					nav.speed = fChaseSpeed;//Sets the speed of the object to be equal to the chase speed.
				}
				else//If the charge timer is less than zero execute the following code.
				{
					fWaitTime = 4.0f;//Set the wait timer to be equal to 4.

					fChargeTime = 6.0f;//Set the charge timer to be equal to 6.
				}
			}
			else//If the wait timer is more than zero execute the following code.
			{
				anim.speed = 0.5f;//Set the speed f the animation of the object to be 0.5.

				nav.speed = 0.0f;//Set the speed of the object to be zero.
			}

			float fDistance = Vector3.Distance(transform.position, player.transform.position);//Sets fDistance to be equal to the distance between the object and the player.

			if (fDistance > fDetectionDistance)//If the distance to the player is greater than the detection distance execute the following code.
			{
				state = "restart";//Set the current state to be equal to the restart case.

				anim.speed = 1.0f;//Set the animation speed to be equal to 1.
			}
		}

		if (state == "restart")//If the current state is equal to restart.
		{
			nav.speed = fWanderSpeed;//Set the speed of the object to be equal to the wander speed.

			nav.destination = v3StartPos;//Set the destination of the AI to be equal to the starting position of the AI.

			float fDistance = Vector3.Distance(transform.position, v3StartPos);//Sets distance to be equal to the distance between the AI and its original position.

			if (fDistance < 3.0f)//If the distance between the AI and it's original position is less than 3 execute the following code.
			{
				state = "strafe";//Sets the current state equal to the strafe state.

				anim.speed = 1.0f;//Sets the animation speed equal to 1.
			}
		}

		#endregion
	}
}