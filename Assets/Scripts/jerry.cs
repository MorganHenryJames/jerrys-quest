﻿//\===========================================================================================
//\ File: jerry.cs
//\ Author: Morgan James
//\ Date Created: 13/05/2017
//\ Brief: Keep jerry in the ball and looking in the correct direction.
//\===========================================================================================

using UnityEngine;

public class jerry : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject ball;//The game object that is the player.

	private GameObject jerryModel;//The game object that is jerry himself.

	public float speedNeeded = 2.0f;//How fast the player must be moving for the dust particle effect to appear.

	private string state = "forward";//Case statement point of entry.

	public ParticleSystem jerrydust;//The effect that will be played if the player is moving fast enough and touching the ground.

	private ParticleSystem.EmissionModule emissMod;//The Emission Module.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		emissMod = jerrydust.emission;//Sets the emission module to effect jerry's dust particle.
		ball = GameObject.Find("Player");//Sets the player to be equal to the game object of the Player.
		jerryModel = GameObject.Find("jerry");//Sets the jerryModel to be equal to the game object of jerry.

	}

	void Update()
	{
		if (ball.GetComponent<Rigidbody>().velocity.magnitude > speedNeeded && ball.GetComponent<playerController>().IsGroundedByRaycast())//If the player's velocity is above speed needed and is also touching the ground.
		{
			emissMod.enabled = true;//Allow the dust particle effect to be emitted.
		}
		else//If jerry is not going fast enough or is not touching the ground.
		{
			emissMod.enabled = false;//Disable the dust particle effect to be emitted.
		}

		jerryModel.transform.position = new Vector3(ball.transform.position.x, ball.transform.position.y - 0.3f, ball.transform.position.z);//Sets the jerry model position equal to the balls position.

		if (state == "forward")//If in the forward state.
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0);//Make jerry face the forward of the camera on the y axis.

			if (Input.GetKey("a"))//If a is being pressed execute the following code.
			{
				state = "left";//Enter the left state.
			}

			if (Input.GetKey("s"))//If s is being pressed execute the following code.
			{
				state = "backwards";//Enter the backwards state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("w") && Input.GetKey("a"))//If w and a are being pressed execute the following code.
			{
				state = "forwardLeft";//Enter the forwardLeft state.
			}

			if (Input.GetKey("a") && Input.GetKey("s"))//If s and a are being pressed execute the following code.
			{
				state = "leftBack";//Enter the leftBack state.
			}

			if (Input.GetKey("s") && Input.GetKey("d"))//If d and s are being pressed execute the following code.
			{
				state = "rightBack";//Enter the rightBack state.
			}

			if (Input.GetKey("d") && Input.GetKey("w"))//If d and w are being pressed execute the following code.
			{
				state = "forwardRight";//Enter the forwardRight state.
			}
		}

		if (state == "backwards")
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y + 180, 0);//Make jerry face the backwards of the camera on the y axis.

			if (Input.GetKey("w"))//If w is being pressed execute the following code.
			{
				state = "forward";//Enter the forward state.
			}

			if (Input.GetKey("a"))//If a is being pressed execute the following code.
			{
				state = "left";//Enter the left state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("w") && Input.GetKey("a"))//If w and a are being pressed execute the following code.
			{
				state = "forwardLeft";//Enter the forwardLeft state.
			}

			if (Input.GetKey("a") && Input.GetKey("s"))//If s and a are being pressed execute the following code.
			{
				state = "leftBack";//Enter the leftBack state.
			}

			if (Input.GetKey("s") && Input.GetKey("d"))//If d and s are being pressed execute the following code.
			{
				state = "rightBack";//Enter the rightBack state.
			}

			if (Input.GetKey("d") && Input.GetKey("w"))//If d and w are being pressed execute the following code.
			{
				state = "forwardRight";//Enter the forwardRight state.
			}
		}

		if (state == "left")
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y + 270, 0);//Make jerry face the left of the camera on the y axis.

			if (Input.GetKey("w"))//If w is being pressed execute the following code.
			{
				state = "forward";//Enter the forward state.
			}

			if (Input.GetKey("s"))//If s is being pressed execute the following code.
			{
				state = "backwards";//Enter the backwards state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("w") && Input.GetKey("a"))//If w and a are being pressed execute the following code.
			{
				state = "forwardLeft";//Enter the forwardLeft state.
			}

			if (Input.GetKey("a") && Input.GetKey("s"))//If s and a are being pressed execute the following code.
			{
				state = "leftBack";//Enter the leftBack state.
			}

			if (Input.GetKey("s") && Input.GetKey("d"))//If d and s are being pressed execute the following code.
			{
				state = "rightBack";//Enter the rightBack state.
			}

			if (Input.GetKey("d") && Input.GetKey("w"))//If d and w are being pressed execute the following code.
			{
				state = "forwardRight";//Enter the forwardRight state.
			}
		}

		if (state == "right")
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y + 90, 0);//Make jerry face the right of the camera on the y axis.

			if (Input.GetKey("w"))//If w is being pressed execute the following code.
			{
				state = "forward";//Enter the forward state.
			}

			if (Input.GetKey("a"))//If a is being pressed execute the following code.
			{
				state = "left";//Enter the left state.
			}

			if (Input.GetKey("s"))//If s is being pressed execute the following code.
			{
				state = "backwards";//Enter the backwards state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("w") && Input.GetKey("a"))//If w and a are being pressed execute the following code.
			{
				state = "forwardLeft";//Enter the forwardLeft state.
			}

			if (Input.GetKey("a") && Input.GetKey("s"))//If s and a are being pressed execute the following code.
			{
				state = "leftBack";//Enter the leftBack state.
			}

			if (Input.GetKey("s") && Input.GetKey("d"))//If d and s are being pressed execute the following code.
			{
				state = "rightBack";//Enter the rightBack state.
			}

			if (Input.GetKey("d") && Input.GetKey("w"))//If d and w are being pressed execute the following code.
			{
				state = "forwardRight";//Enter the forwardRight state.
			}
		}

		if (state == "forwardRight")
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y + 45, 0);//Make jerry face the forward right of the camera on the y axis.

			if (Input.GetKey("w"))//If w is being pressed execute the following code.
			{
				state = "forward";//Enter the forward state.
			}

			if (Input.GetKey("a"))//If a is being pressed execute the following code.
			{
				state = "left";//Enter the left state.
			}

			if (Input.GetKey("s"))//If s is being pressed execute the following code.
			{
				state = "backwards";//Enter the backwards state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("w") && Input.GetKey("a"))//If w and a are being pressed execute the following code.
			{
				state = "forwardLeft";//Enter the forwardLeft state.
			}

			if (Input.GetKey("a") && Input.GetKey("s"))//If s and a are being pressed execute the following code.
			{
				state = "leftBack";//Enter the leftBack state.
			}

			if (Input.GetKey("s") && Input.GetKey("d"))//If d and s are being pressed execute the following code.
			{
				state = "rightBack";//Enter the rightBack state.
			}
		}

		if (state == "forwardLeft")
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y + 315, 0);//Make jerry face the forward left of the camera on the y axis.

			if (Input.GetKey("w"))//If w is being pressed execute the following code.
			{
				state = "forward";//Enter the forward state.
			}

			if (Input.GetKey("a"))//If a is being pressed execute the following code.
			{
				state = "left";//Enter the left state.
			}

			if (Input.GetKey("s"))//If s is being pressed execute the following code.
			{
				state = "backwards";//Enter the backwards state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("a") && Input.GetKey("s"))//If s and a are being pressed execute the following code.
			{
				state = "leftBack";//Enter the leftBack state.
			}

			if (Input.GetKey("s") && Input.GetKey("d"))//If d and s are being pressed execute the following code.
			{
				state = "rightBack";//Enter the rightBack state.
			}

			if (Input.GetKey("d") && Input.GetKey("w"))//If d and w are being pressed execute the following code.
			{
				state = "forwardRight";//Enter the forwardRight state.
			}
		}

		if (state == "rightBack")
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y + 135, 0);//Make jerry face the backwards right of the camera on the y axis.

			if (Input.GetKey("w"))//If w is being pressed execute the following code.
			{
				state = "forward";//Enter the forward state.
			}

			if (Input.GetKey("a"))//If a is being pressed execute the following code.
			{
				state = "left";//Enter the left state.
			}

			if (Input.GetKey("s"))//If s is being pressed execute the following code.
			{
				state = "backwards";//Enter the backwards state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("w") && Input.GetKey("a"))//If w and a are being pressed execute the following code.
			{
				state = "forwardLeft";//Enter the forwardLeft state.
			}

			if (Input.GetKey("a") && Input.GetKey("s"))//If s and a are being pressed execute the following code.
			{
				state = "leftBack";//Enter the leftBack state.
			}

			if (Input.GetKey("d") && Input.GetKey("w"))//If d and w are being pressed execute the following code.
			{
				state = "forwardRight";//Enter the forwardRight state.
			}
		}

		if (state == "leftBack")
		{
			jerryModel.transform.rotation = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y + 225, 0);//Make jerry face the backwards left of the camera on the y axis.

			if (Input.GetKey("w"))//If w is being pressed execute the following code.
			{
				state = "forward";//Enter the forward state.
			}

			if (Input.GetKey("a"))//If a is being pressed execute the following code.
			{
				state = "left";//Enter the left state.
			}

			if (Input.GetKey("s"))//If s is being pressed execute the following code.
			{
				state = "backwards";//Enter the backwards state.
			}

			if (Input.GetKey("d"))//If d is being pressed execute the following code.
			{
				state = "right";//Enter the right state.
			}

			if (Input.GetKey("w") && Input.GetKey("a"))//If w and a are being pressed execute the following code.
			{
				state = "forwardLeft";//Enter the forwardLeft state.
			}

			if (Input.GetKey("s") && Input.GetKey("d"))//If d and s are being pressed execute the following code.
			{
				state = "rightBack";//Enter the rightBack state.
			}

			if (Input.GetKey("d") && Input.GetKey("w"))//If d and w are being pressed execute the following code.
			{
				state = "forwardRight";//Enter the forwardRight state.
			}
		}

	}

	#endregion

}
