﻿//\===========================================================================================
//\ File: platformMovement.cs
//\ Author: Morgan James
//\ Date Created: 26/04/2017	
//\ Brief: Attached to a platform so it can move to different way-points and also scale and rotate to match them.
//\===========================================================================================

using UnityEngine;

public class platformMovement : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public Transform[] waypoints;//The way points that the platform will move and scale and rotate towards.

	public float fMoveSpeed = 4.0f;//How fast the platform will move.

	public float fRotateSpeed = 0.5f;//How fast the platform will rotate.

	public float fScaleSpeed = 0.0f;//How fast the platform will scale.

	private int iCurrentPoint = 0;//The current way point the platform is moving towards.

	private bool bReverse = false;//A boolean value to indicate whether or not the platform is going forwards or backwards through the array.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void FixedUpdate()
	{
		if (transform.position != waypoints[iCurrentPoint].transform.position)//If the platform is not at the way point it is heading towards execute the following code.
		{
			transform.position = Vector3.MoveTowards(transform.position, waypoints[iCurrentPoint].transform.position, fMoveSpeed * Time.deltaTime);//Moves the platform towards the current way point.

			transform.rotation = Quaternion.Lerp(transform.rotation, waypoints[iCurrentPoint].transform.rotation, fRotateSpeed * Time.deltaTime);//Rotates the platform to match the current way point's rotation.

			transform.localScale = Vector3.Lerp(transform.localScale, waypoints[iCurrentPoint].transform.lossyScale, fScaleSpeed * Time.deltaTime);//Scales the platform to match the current way point's scale
		}

		if (bReverse == true)//If the reverse boolean is true execute the following code.
		{
			if (transform.position == waypoints[iCurrentPoint].transform.position)//If the platform is at the current way point's position execute the following code.
			{
				iCurrentPoint -= 1;//Decrease the current way point.
			}

			if (iCurrentPoint < 0)//If the current way point is at the first way point execute the following code.
			{
				iCurrentPoint += 1;//Increase the current way point.

				bReverse = false;//Set the reverse boolean to be false.
			}
		}
		else//If the reverse boolean is false execute the following code.
		{
			if (transform.position == waypoints[iCurrentPoint].transform.position)//If the platform is at the current way point's position execute the following code.
			{
				iCurrentPoint += 1;
			}

			if (iCurrentPoint >= waypoints.Length)//If the platform is at the last way point execute the following code.
			{
				iCurrentPoint -= 1;//Decrease the current way point.

				bReverse = true;//Set the reverse boolean to be true.
			}
		}
	}

	#endregion

}
