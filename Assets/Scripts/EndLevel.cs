//\===========================================================================================
//\ File: endLevel.cs
//\ Author: Morgan James
//\ Date Created: 21/02/2017
//\ Brief: When the play enters the attached object load the linked scene.
//\===========================================================================================

using UnityEngine;
using UnityEngine.SceneManagement;

public class endLevel : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public string levelToLoad;

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			SceneManager.LoadScene(levelToLoad);//Loads the scene specified.
		}
	}

	#endregion
}
