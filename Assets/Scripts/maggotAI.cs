﻿//\===========================================================================================
//\ File: maggotAI.cs
//\ Author: Morgan James
//\ Date Created: 12/05/2017
//\ Brief: AI for a maggot to wander around and then if a player is near to follow the player.
//\===========================================================================================

using UnityEngine;

public class maggotAI : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject player;//The game object that is the player.

	private string state = "strafe";//The entry point to the case statement.

	private NavMeshAgent nav;//The navigation mesh of the object.

	public float fWanderDistance = 3.0f;//How far the AI can wander around.

	public float fChaseSpeed = 5.0f;//How fast the AI can move when in the chase case.

	public float fWanderSpeed = 3.0f;//How fast the AI can move when wandering.

	private Vector3 v3StartPos;//The starting position of the AI.

	public float fDetectionDistance = 10.0f;//How far the AI can see/detect.

	private Animator anim;//The object animator.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		v3StartPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);//Sets the starting position equal to the objects transform

		player = GameObject.Find("Player");//Sets the player to be equal to the game object of the Player.

		nav = GetComponent<NavMeshAgent>();//Sets the navigation agent to the objects attached navigation agent.

		anim = GetComponent<Animator>();//Sets the animator to the objects attached animator.
	}

	void Update()
	{

		if (state == "strafe")//If in the strafe state.
		{
			nav.speed = fWanderSpeed;//Set the speed of the AI to the wandering speed.

			Vector3 randomPos = Random.insideUnitSphere * fWanderDistance;//Picks a random spot in a sphere with radius of wander distance.

			NavMeshHit navHit;//Creates a position of the navigation mesh to go to. 

			NavMesh.SamplePosition(transform.position + randomPos, out navHit, 20f, NavMesh.AllAreas);//Sets the sample position of the navigation mesh.

			float fDistance = Vector3.Distance(transform.position, player.transform.position);//Sets the fDistance to be equal to the distance between the AI and the player.

			if (fDistance < fDetectionDistance)//If the distance to the player is smaller than the detection distance execute the following code.
			{
				state = "chase";//Set the current case to be equal to the chase case.

				anim.speed = 3.5f;//Sets speed of the animation to a faster one.
			}

			nav.SetDestination(navHit.position);//Sets the destination of the AI.
		}

		if (state == "chase")//If in the chase state.
		{
			nav.speed = fChaseSpeed;//Set the speed of the AI to the chasing speed.

			nav.destination = player.transform.position;//Sets the destination of the AI to be the player.

			float fDistance = Vector3.Distance(transform.position, player.transform.position);//Sets the fDistance to be equal to the distance between the AI and the player.

			if (fDistance > fDetectionDistance)//If the distance to the player is greater than the detection distance then execute the following code.
			{
				state = "restart";//Set the current case to be equal to the restart case.

				anim.speed = 1.2f;//Sets the speed of the animation to be a slower one.
			}
		}

		if (state == "restart")//If in the restart state.
		{
			nav.speed = fWanderSpeed;//Set the speed of the AI to be equal to the wandering speed.

			nav.destination = v3StartPos;//Set the destination of the AI to its original position.

			float fDistance = Vector3.Distance(transform.position, nav.destination);//Sets the fDistance to be equal to the distance between the AI and the player.

			if (fDistance < 3.0f)//If the distance between the AI and the original position is less than 3.0f execute the following code.
			{
				state = "strafe";//Set the current case to be equal to strafe.

				anim.speed = 1.0f;//Set the animation speed to be a slower one.
			}
		}

		#endregion
	}

}
