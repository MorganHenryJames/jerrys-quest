//\===========================================================================================
//\ File: playerController.cs
//\ Author: Morgan James
//\ Date Created: 21/02/2017
//\ Brief: Adds velocity in accordance to what button is pressed along with controls for jumping and charging.
//\===========================================================================================

using UnityEngine;

public class playerController : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public float fMovementSpeed = 400;//How much force is added with each tick of game time whilst a movement key is being held.

	public float fJumpSpeed = 300;//How much force is added in the up direction to the player when space is pressed.

	public float fChargeSpeed = 500;//How much force is added to the player in the forward direction of the camera when r is pressed after attaining the charge candy.

	public bool bIsGrounded = false;//A boolean value to indicate if the player has ground beneath.

	private float fDistance = 1.0f;//The distance checked for ground beneath the player.

	public RaycastHit hit;//Holds information from ray cast.

	public Vector3 v3Direction = new Vector3(0f, -1f, 0f);//The direction of the ray cast which is directly down.

	private Transform cameraTransform;//The main camera's transform.

	private GameObject empty;//A game object that will be equal to the permanent variables object.

	private float jumpTimer = 0.0f;//The float that contains how long until the player will be able to jump again.

	public float chargeResetTime = 5.0f;//The float that contains how long until the player will be able to charge again.

	public Animator anim;//The animator that this script will effect.

	public AudioClip[] jumpSounds;//The audio clips that will be played whenever the player jumps.

	public AudioClip chargeSound;//The audio clip that will play when the player charges.

	private AudioSource audioSource;//The audioSource that audio will play fro.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		empty = GameObject.Find("Permanent Variables");//Sets the empty game object equal to the game object that contains the variables that wont be destroyed on scene change.

		cameraTransform = GameObject.Find("mainCamera").transform;//Sets the transform equal to the main camera's transform.

		audioSource = GetComponent<AudioSource>();//Sets the audio source equal to the one attached to the same object this script is attached to.
	}

	void FixedUpdate()
	{
		//\===========================================================================================
		//\ Movement
		//\===========================================================================================

		Vector3 movementVector = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));//Gets the input from the user and converts it into a vector.

		movementVector = cameraTransform.rotation * movementVector;//multiplies the camera rotation by the movement vector to allow force application at the correct point.

		GetComponent<Rigidbody>().AddForce(movementVector * fMovementSpeed * Time.deltaTime);//Applies the movement vector multiplied by the move speed to the player.

		//\===========================================================================================
		//\ Animation
		//\===========================================================================================

		if (Input.GetKey("w") || Input.GetKey("a") || Input.GetKey("s") || Input.GetKey("d") || Input.GetKeyDown("r"))//Checks if any movement input keys are being held or if the charge was pressed and executes the following code.
		{
			anim.SetBool("isRunning", true);//Sets the running animation of jerry to play.
		}
		else//If there is no movement keys being pressed execute the following code.
		{
			anim.SetBool("isRunning", false);//Disables the running animation of jerry.
		}

		//\===========================================================================================
		//\ Jumping
		//\===========================================================================================

		jumpTimer -= Time.deltaTime;//Decreases the jump timer.

		if (jumpTimer <= 0)//If the jump timer is less than or equal to zero execute the following code.
		{
			jumpTimer = 0;//Stops the jump timer counting infinitely down.
		}

		if (jumpTimer > 0)//If the jump timer is more than zero execute the following code.
		{
			//Do nothing.
		}
		else if (Input.GetKeyDown("space") && IsGroundedByRaycast())//If the jump timer is not above zero and space is being pressed and is grounded execute the following code.
		{
			int random = Random.Range(0, jumpSounds.Length);//Set an integer equal to a number from zero to the length of the jump sounds array.

			audioSource.PlayOneShot(jumpSounds[random]);//Play a random sound from the jump sounds array.

			GetComponent<Rigidbody>().AddForce(Vector3.up * fJumpSpeed);//Adds force to the player in the up direction multiplied by the jump speed.

			jumpTimer = 0.7f;//Sets the jump timer so that the player may not jump again before the ray cast checker has time to move away from the ground(prevents double jumps from two button presses in quick succession)
		}

		//\===========================================================================================
		//\ Charging
		//\===========================================================================================

		if (Input.GetKeyDown("r") && CanCharge())//If the button r is pressed and canCharge is true execute the following code.
		{
			audioSource.PlayOneShot(chargeSound);//Play the charge audio clip.

			GetComponent<Rigidbody>().AddForce(new Vector3(cameraTransform.forward.x, 0.0f, cameraTransform.forward.z) * fChargeSpeed);//Add force to the player in the direction of the camera forward multiplied the charge speed.

			empty.GetComponent<variableHolder>().fChargeTimer = chargeResetTime;//Sets the charge timer equal to the chargeResetTime.
		}
	}

	public bool CanCharge()//Checks if the player can charge or not.
	{
		if (empty.GetComponent<variableHolder>().bChargeCandy == true && empty.GetComponent<variableHolder>().fChargeTimer == 0)//If the player has picked up the charge candy and the charge timer is equal to zero execute the following code.
		{
			return true;
		}
		else//If the player doesn't have the charge candy or the charge timer is above zero execute the following code.
		{
			return false;
		}
	}

	//\===========================================================================================
	//\ Grounded Check
	//\===========================================================================================

	public bool IsGroundedByRaycast()//Checks if the player is grounded by way of ray cast.
	{
		if (Physics.Raycast(transform.position, v3Direction, out hit, fDistance))//If the ray cast hits a rigid body execute the following code.
		{
			return true;
		}
		return false;
	}

	#endregion

}


