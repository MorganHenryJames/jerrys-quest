﻿//\===========================================================================================
//\ File: springSprung.cs
//\ Author: Morgan James
//\ Date Created: 05/05/2017
//\ Brief: Waits for a period of time then replaces itself with an un-sprung version.
//\===========================================================================================

using UnityEngine;

public class spingSprung : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public GameObject unsprung;//The object that will be created on the destruction of this object.

	public float fSpringTime = 2.0f;//How long the sprung spring exists.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Update()
	{
		fSpringTime -= Time.deltaTime;//Decreases the time that dictates the springs remaining time left to exist.

		if (fSpringTime <= 0)//If the spring time is less than or equal to 0 execute the following code.
		{
			var instantiatedPrefab = Instantiate(unsprung, transform.position, transform.rotation) as GameObject;//Create a un-sprung spring at the position of the object.

			instantiatedPrefab.transform.localScale = transform.localScale;//Sets the scale of the new object to be equal to the current objects scale.

			Destroy(gameObject);//Destroys this game object.
		}
	}

	#endregion

}
