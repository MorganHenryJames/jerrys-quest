﻿//\===========================================================================================
//\ File: resetHighScore.cs
//\ Author: Morgan James
//\ Date Created: 10/05/2017
//\ Brief: Deletes the variable saved to the device that contains the High score but since thats the only variable saved delete all is used.
//\===========================================================================================

using UnityEngine;

public class resetHighScore : MonoBehaviour
{
	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	public void Reset()//A function to delete all device saved variables.
	{
		PlayerPrefs.DeleteAll();//Deletes all variables saved to the device.
	}

	#endregion

}
