﻿//\===========================================================================================
//\ File: bossAI.cs
//\ Author: Morgan James
//\ Date Created: 13/05/2017
//\ Brief: Makes the object it is applied to look at the player and shoot objects from a spawn point.
//\===========================================================================================

using UnityEngine;

public class bossAI : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private Transform player;//The players transform to lock onto.

	public GameObject itemSpawnPoint;//The game object that the projectiles will shoot from.

	public Rigidbody[] projectilePrefabs;//An array of rigid bodies that will become projectiles.

	public float fFireRate = 5.0f;//The delay between shots.

	public float fProjectileSpeed = 200.0f;//How fast the projectiles will travel.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		player = GameObject.Find("Player").transform;//Sets the player transform to be equal to the game object of the Player.
	}

	void FixedUpdate()
	{
		var lookPos = player.position - transform.position;//Where the object will be looking.

		lookPos.y = 0;//Stops the object looking up or down.

		var rotation = Quaternion.LookRotation(lookPos);//Creates a rotation with the specified forward and upward directions.

		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);//Sets the rotation of the object to rotate from its own position to the newly defined rotation each frame.

		fFireRate -= Time.deltaTime;//Decreases fire rate by delta time.

		if (fFireRate <= 0)//Checks to see if fire rate is less than or equal to 0. If so executes the following code.
		{
			int random = Random.Range(0, projectilePrefabs.Length);//Sets a random int between 0 and the amount of projectiles in the projectile array.

			Rigidbody hitPlayer;//Creates a local variable hitPlayer that is a rigid body.

			hitPlayer = Instantiate(projectilePrefabs[random], itemSpawnPoint.transform.position, itemSpawnPoint.transform.rotation) as Rigidbody;//Creates a projectile at the item spawn point with the same rotation as the spawn point.

			hitPlayer.velocity = transform.TransformDirection(Vector3.forward * fProjectileSpeed);//Sets the velocity of the projectile to be equal to the objects forward multiplied by the projectile speed.

			fFireRate = 5;//Resets the fire rate timer.
		}
	}

	#endregion

}
