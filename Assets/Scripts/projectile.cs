﻿//\===========================================================================================
//\ File: projectile.cs
//\ Author: Morgan James
//\ Date Created: 14/05/2017
//\ Brief: Attached to projectiles. Allows for visual and audio interactions between the player and the projectile.
//\ Destroys the projectile after a random time period.
//\===========================================================================================

using UnityEngine;

public class projectile : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public float fTimerTopRange = 20.0f;//The longest amount of time the object can exist.

	public float fTimerBottomRange = 10.0f;//The shortest amount of time the object can exist.

	private float fTimer = 0.0f;//The amount of time the object will exist for.

	public AudioClip[] hits;//The audio clips that will be played if the object collides with the player.

	private AudioSource audioSource;//The audio source the sounds will play from.

	public ParticleSystem hitEffect;//The particle effect that will spawn when the projectile collides with the player.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		audioSource = GetComponent<AudioSource>();//Sets the audio source to be equal to the audio source attached to the object.
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			int random = Random.Range(0, hits.Length);//Sets an integer to be a number from zero to the length of the hit sound array.

			audioSource.PlayOneShot(hits[random]);//Plays a random audio clip from the hit sound array.

			Instantiate(hitEffect, other.transform.position, other.transform.rotation);//Creates a hit particle effect at the player position.
		}
	}

	void Update()
	{
		fTimer = Random.Range(fTimerBottomRange, fTimerTopRange);//Sets the timer to be a random float between the bottom range and top range.

		Destroy(gameObject, fTimer);//Destroys the game object after the amount of time contained in timer passes.
	}

	#endregion

}
