﻿//\===========================================================================================
//\ File: maggotCollider.cs
//\ Author: Morgan James
//\ Date Created: 12/05/2017
//\ Brief: Attached to the hit box of the maggotAI to destroy the maggot when hit and drop a key if selected to do so.
//\===========================================================================================

using UnityEngine;

public class maggotCollider : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject empty;//A game object that will be equal to the permanent variables object.

	public GameObject keyPrefab;//The game object that will be dropped if the keyDrop boolean is true.

	public ParticleSystem deathEffect;//The particle effect that will be played on the death of the maggot.

	public bool keyDrop = false;//If set to true the maggot will drop a Key on death.

	public AudioClip deathSound;//The sound played when a maggot dies.

	private AudioSource audioSource;//The audio source the death sound will play from.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		empty = GameObject.Find("Permanent Variables");//Sets the empty game object equal to the game object that contains the variables that wont be destroyed on scene change.

		audioSource = GameObject.Find("Player").GetComponent<AudioSource>();//Sets the audio source to the player as when the maggot dies it will not be able to play sounds.
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			audioSource.PlayOneShot(deathSound);//Play the death sound 

			if (keyDrop == true)//If the keyDrop boolean is set to true execute the following code.
			{
				Instantiate(keyPrefab, transform.parent.position, transform.parent.rotation);//Creates a game object of the key at the position of the parent of this object.
			}

			Instantiate(deathEffect, transform.position, transform.rotation); ;//Creates an instance of the death particle effect at the players position.

			empty.GetComponent<variableHolder>().SetCountText(2);//Increases the score by two.

			Destroy(transform.parent.gameObject);//Destroys the parent of this game object.

		}
	}

	#endregion

}
