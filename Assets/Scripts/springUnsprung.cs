﻿//\===========================================================================================
//\ File: springUnsprung.cs
//\ Author: Morgan James
//\ Date Created: 14/05/2017
//\ Brief: Attached to the spring object. Allow the player to collide with it and add force to the player.
//\ Also creates the sprung version of the spring whilst simultaneously destroying itself.
//\===========================================================================================

using UnityEngine;

public class springUnsprung : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public float fIntensity = 10f;//How much force will be added in the up direction to the player.

	public GameObject destroyedVersion;//The object that will replace this one when hit by the player.

	public AudioClip springSound;//The audio clip that will play when the spring is sprung.

	public AudioSource audioSource;//The audio source from which the sound will play from.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods
	void Start()
	{
		audioSource = GameObject.Find("Player").GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			other.GetComponent<Rigidbody>().velocity = other.GetComponent<Rigidbody>().velocity + transform.up * fIntensity;//Sets the velocity of the player to be equal to the current velocity + the up transform multiplied by intensity.

			var instantiatedPrefab = Instantiate(destroyedVersion, transform.position, transform.rotation) as GameObject;//Creates a sprung version of this object at the same location.

			instantiatedPrefab.transform.localScale = transform.localScale;//Scales the new sprung object to the scale of this object.

			audioSource.PlayOneShot(springSound);//Plays the audio clip of the spring sound.

			Destroy(gameObject);//Destroys the game object.
		}
	}

	#endregion

}
