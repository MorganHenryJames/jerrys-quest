﻿//\===========================================================================================
//\ File: pipePortal.cs
//\ Author: Morgan James
//\ Date Created: 19/04/2017 
//\ Brief: Allows for two objects to be linked and for the player to travel too and from them.
//\===========================================================================================

using UnityEngine;

public class pipePortal : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public Transform otherPortal;//The other object that the player will be moved to.

	public float fTeleportOffset = 2.0f;//How far away the player will be moved to away from the desired destination due being moved to inside the hit box of the other portal.

	public AudioClip[] pipeSounds;//The sounds that will play when a player uses a pipe/portal.

	private AudioSource audioSource;//The audio source that the sound will be played from.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		audioSource = GetComponent<AudioSource>();//Sets the audio source to be equal to the audio source attached to the object.
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			int random = Random.Range(0, pipeSounds.Length);//Sets an integer to be equal to a number in the range of 0 to the length of the pipesSunds array.

			audioSource.PlayOneShot(pipeSounds[random]);//Play a random sound from the pipeSounds array.

			other.GetComponent<Rigidbody>().velocity = otherPortal.forward * other.GetComponent<Rigidbody>().velocity.magnitude;//Sets the velocity of the player to be equal to the forward of the object it's getting moved to multiplied by its current velocity magnitude.

			other.transform.position = otherPortal.position + otherPortal.forward * fTeleportOffset;//Moves the player to the other portal/object plus the off set multiplied by the forward of that object.
		}
	}

	#endregion

}
