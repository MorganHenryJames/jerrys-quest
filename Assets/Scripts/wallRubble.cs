﻿//\===========================================================================================
//\ File: wallRubble.cs
//\ Author: Morgan James
//\ Date Created: 14/05/2017
//\ Brief: Attached to the parts of the broken walls rubble.
//\===========================================================================================

using UnityEngine;
using UnityEngine.SceneManagement;

public class wallRubble : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public float timerTopRange = 8.0f;//The longest time this object may exist.

	public float timerBottomRange = 4.0f;//The shortest time this object may exist.

	private float timer = 0.0f;//The time that this object will exist for.

	public Material level2Mat;//The material this object will be when the second scene is loaded.

	public Material level3Mat;//The material this object will be when the third scene is loaded.

	public Material level4Mat;//The material this object will be when the fourth scene is loaded.

	public Material level5Mat;//The material this object will be when the fifth scene is loaded.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Update()
	{
		switch (SceneManager.GetActiveScene().buildIndex)//Sets the case to be equal to the loaded scene in accordance to its index.
		{
			case 3://If the third scene is loaded.
				GetComponent<Renderer>().material = level2Mat;//Sets the material of this object to the level2Mat.
				break;

			case 4://If the fourth scene is loaded.
				GetComponent<Renderer>().material = level3Mat;//Sets the material of this object to the level3Mat.
				break;

			case 5://If the fifth scene is loaded.
				GetComponent<Renderer>().material = level4Mat;//Sets the material of this object to the level4Mat.
				break;

			case 6://If the sixth scene is loaded.
				GetComponent<Renderer>().material = level5Mat;//Sets the material of this object to the level5Mat
				break;
		}

		timer = Random.Range(timerBottomRange, timerTopRange);//Sets the timer to be equal to a random number between the bottom range and the top range.

		Destroy(gameObject, timer);//Destroys the object after a period of time equal to the timer.
	}

	#endregion

}
