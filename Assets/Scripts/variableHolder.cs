﻿//\===========================================================================================
//\ File: collectible.cs
//\ Author: Morgan James
//\ Date Created: 21/02/2017
//\ Brief: Increases the score and sets the pickup to false when player collides with it.
//\===========================================================================================

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class variableHolder : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public Text VelocityText;//The UI text that displays the speed.

	public Text scoreText;//The UI text that displays the score.

	private int iScore = 0;//The score of the player.

	public int iCachedscore = 0;//The score that is cached for the final level.

	public int iKeys = 0;//The amount of keys that the player has collected.

	private float fTime = 0.0f;//The amount of time passed since the start of the game.

	public float fCachedtime = 0.0f;//The cached time for the final level.

	public Text TimeText;//The UI text that displays the time passed since the start of the game.

	public bool bChargeCandy = false;//A boolean value that indicates whether or the player has collected the charge candy.

	public float fChargeTimer = 0.0f;//The time remaining for the cool down of the use of the charge ability.

	public Text ChargeText;//The UI text that displays the cool down of the charge ability.

	public Text highscore;//The UI text that displays the high score of the game.

	public Text endHighScoreTime;//The UI text that displays the high score of the game on the end level scene.

	public Text thisGameTime;//The UI text that displays the time of the run through on the end level scene.

	public Text thisGameScore;//The UI text that displays the score of the run through on the end level scene.

	public Text endHighScore;//The UI text that displays the high score on the end scene.

	public Text menuhighScore;//The UI text that displays the high score on the menu.

	public Text menuhighScoreTime;//The UI text that displays the high score time on the menu.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		highscore.text = "High Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();//Sets the high score UI text equal to "High Score: " + the high score.

		scoreText.text = "Score: " + iScore.ToString();//Sets the score UI text equal to "Score: " + score.
	}

	public void SetCountText(int increment)//Used to increase the score of the game.
	{
		iScore += increment;//Increases the score of the game.

		scoreText.text = "Score: " + iScore.ToString();//Sets the UI text of score to be equal to "Score: " + score.
	}

	public void SetVelocityText(float velo)//Sets velocity text.
	{
		VelocityText.text = "Speed: " + velo.ToString();//Sets the UI text for velocity to "Speed: " + the velocity of the player.
	}

	void FixedUpdate()
	{
		if (SceneManager.GetActiveScene().name == "Menu")//If the Menu scene is loaded execute the following code.
		{
			TimeText.GetComponent<Text>().enabled = false;//Disables the time text UI element.

			highscore.GetComponent<Text>().enabled = false;//Disables the high score text UI element.

			VelocityText.GetComponent<Text>().enabled = false;//Disables the velocity text UI element.

			ChargeText.GetComponent<Text>().enabled = false;//Disables the charge text UI element.

			scoreText.GetComponent<Text>().enabled = false;//Disables the score text UI element.

			cacheGame();//Saves the game variables that need to be displayed on the end screen as new variables.

			newGame();//Resets the variables used in the game.

			endHighScore.GetComponent<Text>().enabled = false;//Disables the end high score UI element.

			endHighScoreTime.GetComponent<Text>().enabled = false;//Disables the end high score time UI element.

			thisGameTime.GetComponent<Text>().enabled = false;//Disables the thisGameTime UI element.

			thisGameScore.GetComponent<Text>().enabled = false;//Disables the thisGameScore UI element.

			menuhighScore.GetComponent<Text>().enabled = true;//Enables the menuhighScoreTime UI element.

			menuhighScoreTime.GetComponent<Text>().enabled = true;//Enables the menuhighScoreTime UI element.
		}
		else if (SceneManager.GetActiveScene().name == "endScene")//Otherwise if the endScene is loaded execute the following code.
		{
			TimeText.GetComponent<Text>().enabled = false;//Disables the time text UI element.

			highscore.GetComponent<Text>().enabled = false;//Disables the high score text UI element.

			VelocityText.GetComponent<Text>().enabled = false;//Disables the velocity text UI element.

			ChargeText.GetComponent<Text>().enabled = false;//Disables the charge text UI element.

			scoreText.GetComponent<Text>().enabled = false;//Disables the score text UI element.

			cacheGame();//Saves the game variables that need to be displayed on the end screen as new variables.

			newGame();//Resets the variables used in the game.

			endHighScore.GetComponent<Text>().enabled = true;//Enables the end high score UI element.

			endHighScoreTime.GetComponent<Text>().enabled = true;//Enables the end high score time UI element.

			thisGameTime.GetComponent<Text>().enabled = true;//Enables the thisGameTime UI element.

			thisGameScore.GetComponent<Text>().enabled = true;//Enables the thisGameScore UI element.

			menuhighScore.GetComponent<Text>().enabled = false;//Disables the menuhighScoreTime UI element.

			menuhighScoreTime.GetComponent<Text>().enabled = false;//Disables the menuhighScoreTime UI element.

			thisGameScore.text = "Score: " + iCachedscore.ToString();//Sets the thisGameScore UI element to be equal to "Score: " + score.

			thisGameTime.text = "Time: " + fTime.ToString();//Sets the thisGameTime UI element to be equal to "Time: " + time.

		}
		else//Otherwise on all other scenes execute the following code.
		{
			ChargeText.GetComponent<Text>().enabled = true;//Enables the charge text UI element.

			TimeText.GetComponent<Text>().enabled = true;//Enables the time text UI element.

			highscore.GetComponent<Text>().enabled = true;//Enables the high score UI element.

			VelocityText.GetComponent<Text>().enabled = true;//Enables the velocity text UI element.

			scoreText.GetComponent<Text>().enabled = true;//Enables the scoreText UI element.

			endHighScore.GetComponent<Text>().enabled = false;//Disables the endHighScore UI element.

			endHighScoreTime.GetComponent<Text>().enabled = false;//Disables the endHighScoreTime UI element.

			thisGameTime.GetComponent<Text>().enabled = false;//Disables the thisGameTime UI element.

			thisGameScore.GetComponent<Text>().enabled = false;//Disables the thisGameScore UI element.

			menuhighScore.GetComponent<Text>().enabled = false;//Disables the menuhighScore UI element.

			menuhighScoreTime.GetComponent<Text>().enabled = false;//Disables the menuhighScoreTime UI element.
		}

		if (SceneManager.GetActiveScene().name != "Menu" && SceneManager.GetActiveScene().name != "endScene")//If the active scene is not the Menu and not the endScene execute the following code.
		{
			fTime += Time.deltaTime;//Increases the time.

			TimeText.text = "Time:" + Mathf.Round(fTime);//Sets the UI time text to be equal to "Time: " + time.
		}

		fChargeTimer -= Time.deltaTime;//Decreases the charge timer.

		if (fChargeTimer <= 0)//If the charge timer is less than or equal to 0 execute the following code.
		{
			fChargeTimer = 0;//Set the charge timer to 0.
		}

		if (fChargeTimer > 0)//If the charge timer is larger than 0 execute the following code.
		{
			ChargeText.text = "Can Charge In:" + Mathf.Round(fChargeTimer);//Set the charge UI text to be equal to "Can Charge In:" + cool down of the charge.
		}
		else//If the charge timer is not more than zero.
		{
			ChargeText.text = "";//Set the charge text UI element to be equal to nothing.
		}

		if (iScore >= PlayerPrefs.GetInt("HighScore", 0))//If the score is more than or equal to the high score execute the following code.
		{
			PlayerPrefs.SetInt("HighScore", iScore);//Set the device variable HighScore to be equal to the score.

			PlayerPrefs.SetFloat("HighScoreTime", fTime);//Set the device variable HighScoreTime to be equal to the time.
		}

		highscore.text = "High Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();//Sets the HighScore text to be equal to "High Score: " + HighScore.

		endHighScore.text = "High Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();//Sets the end high score text to be equal to "HighScore: " + HighScore.

		endHighScoreTime.text = "High Score Time: " + PlayerPrefs.GetFloat("HighScoreTime", 0).ToString();//Sets the endHighScoreTime to be equal to "High Score Time: " + high score time.

		menuhighScore.text = "High Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();//Sets the menuhighScore UI text to be equal to the "High Score: " + high score.

		menuhighScoreTime.text = "High Score Time: " + PlayerPrefs.GetFloat("HighScoreTime", 0).ToString();//Sets the menuhighScoreTime UI text element to be equal to "High Score Time: " + high score time.
	}

	public void newGame()//A function to reset the variables used in the game.
	{
		iScore = 0;//Sets the score to zero.

		iKeys = 0;//Sets the keys collected to zero.

		fTime = 0;//Sets the time to zero.

		bChargeCandy = false;//Sets the Charge Candy to false.

		fChargeTimer = 0;//Sets the charge timer to 0.
	}

	public void cacheGame()//A function to cache game variables so that they can be displayed after being reset.
	{
		iCachedscore = iScore;//Sets the cached score equal to the score.

		fCachedtime = fTime;//Sets the cached time equal to the time.
	}

	#endregion

}
