//\===========================================================================================
//\ File: cameraController.cs
//\ Author: Morgan James
//\ Date Created: 21/02/2017
//\ Brief: Allows for the tilting of the camera and rotation along with Lerping.
//\===========================================================================================

using UnityEngine;

public class cameraController : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject player;//The game object that will be equal to the player.

	private Vector3 v3Offset;//The difference between the current position and where it needs to be.

	private float fTiltMax = 12f;//Maximum angle to tilt the camera to fake the level tilting.

	private GameObject newPos;//The game object that will be where the camera is supposed to be.

	private float fRotationDamping = 10f;//How snappy the camera is to rotation.

	private float fMovementDamping = 150f;//How snappy the camera is to movement.

	private float fTurnSpeed = 300f;//How fast the camera turns.

	private float fTurnAngle = 0.0f;//The current turn angle.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		v3Offset = transform.position;//Sets the offset equal to the current camera position.

		newPos = new GameObject("cameraFollow");//Sets newPos equal the a new game object called cameraFollow.

		newPos.transform.position = transform.position;//Sets the position of newPos equal to the current camera position.

		player = GameObject.Find("Player");//Sets the player transform to be equal to the game object of the Player.

		if (player == null)//If there is no player execute the following code.
		{
			Debug.LogError("Could not find object \"Player\" ! Aborting GameCamera load.");//Creates a debug error.

			DestroyImmediate(gameObject);//Destroys the object the script is attached to.
		}
	}

	void Update()
	{
		fTurnAngle += Input.GetAxis("Turn") * fTurnSpeed * Time.deltaTime;//Rotate the camera around the ball to adjust movement when Q or E are pressed (left/right movement).
	}

	void LateUpdate()
	{
		var heading = transform.position - player.transform.position;//Find the ZX direction from the player to the camera.

		heading.y = 0f;//Zero's the y component of heading so it is ignored.

		var distance = heading.magnitude;//Sets the distance equal to the magnitude of heading.

		var direction = heading / distance;//Sets the direction to heading divided by distance.

		var rotationVectorRight = Vector3.Cross(direction, Vector3.up);//Find the right vector for the direction.

		newPos.transform.position = player.transform.position + v3Offset;//Move the camera.

		newPos.transform.RotateAround(player.transform.position, Vector3.up, fTurnAngle);//Rotate around the players Y axis by the turn value.

		newPos.transform.RotateAround(player.transform.position, rotationVectorRight, -Input.GetAxisRaw("Vertical") * fTiltMax);//Deal with forward/backward board rotation.

		newPos.transform.LookAt(player.transform.position);//Ensure we're looking at the player before the roll rotation is applied.

		newPos.transform.RotateAround(newPos.transform.position, direction, -Input.GetAxisRaw("Horizontal") * fTiltMax);//Apply the roll rotation.

		transform.position = Vector3.Slerp(transform.position, newPos.transform.position, Time.deltaTime * fMovementDamping);//Lerp the cameras position to match the target object.

		transform.rotation = Quaternion.Lerp(transform.rotation, newPos.transform.rotation, Time.deltaTime * fRotationDamping);//Lerp the cameras rotation to match the target object.

		centerCameraOnTarget();//Re-center the camera on the object to account for new roll rotation.
	}

	private void centerCameraOnTarget()
	{
		Plane plane = new Plane(transform.forward, player.transform.position);//Creates a plane.

		Ray ray = GetComponent<Camera>().ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0.0f));//Displays a ray.

		float fDistance;//The distance of the ray-cast.

		plane.Raycast(ray, out fDistance);//Creates a ray-cast from the plan.

		var point = ray.GetPoint(fDistance);//Sets point equal to the point at fDistance along the line of the ray.

		var v3Offset = player.transform.position - point;//Sets the off set equal to the player transform negative the point.

		transform.position += v3Offset;//Adds the offset to the position of the camera.
	}

	#endregion

}