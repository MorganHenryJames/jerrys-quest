﻿//\===========================================================================================
//\ File: collectibles.cs
//\ Author: Morgan James
//\ Date Created: 21/02/2017
//\ Brief: Increases the score and destroys the pickup collides with it.
//\===========================================================================================

using UnityEngine;

public class collectibles : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject empty;//A game object that will be equal to the permanent variables object.

	private float fVelocity = 0.0f;//Shows how fast the player will be moving.

	public AudioClip key;//The audio clip that will be played when a key is collected.

	public AudioClip chargeCandy;//The audio clip that will be played when the charge candy is collected.

	public AudioClip[] food;//The audio clips that will be played when a food/candy is collected.

	public ParticleSystem foodEffect;//The particle effect that will play when a food/candy is collected.

	public ParticleSystem keyEffect;//The particle effect that will play when a key is collected.

	private AudioSource audioSource;//The audio source to play the sounds from.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		empty = GameObject.Find("Permanent Variables");//Sets the empty game object equal to the game object that contains the variables that wont be destroyed on scene change.

		audioSource = GetComponent<AudioSource>();//Sets the audio source equal to the one attached to the same object this script is attached to.
	}

	void FixedUpdate()
	{
		fVelocity = GetComponent<Rigidbody>().velocity.magnitude;//Sets the fVeclocity equal to the magnitude of the velocity of the object that this script is attached to.

		empty.GetComponent<variableHolder>().SetVelocityText(fVelocity);//Calls upon the variable holder script function to set the UI speed text to be equal to fVelocity.
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.gameObject.CompareTag("PickUp"))//If the colliding object is a "PickUp".
		{
			int random = Random.Range(0, food.Length);//Sets a random int between 0 and the amount of food sounds in the food sound array.

			audioSource.PlayOneShot(food[random]);//Plays one of the food sound effect.

			Instantiate(foodEffect, transform.position, transform.rotation);//Creates an instance of the food particle effect at the objects position.

			empty.GetComponent<variableHolder>().SetCountText(1);//Calls the SetCountText function to update the score.

			Destroy(other.gameObject);//Destroys the object.

		}
		if (other.gameObject.CompareTag("demo"))//If the colliding object is a "demo" key.
		{
			audioSource.PlayOneShot(key, 1.0F);//Plays the Key sound effect.

			Instantiate(keyEffect, transform.position, transform.rotation);//Creates an instance of the key particle effect at the objects position.

			empty.GetComponent<variableHolder>().iKeys += 5;//Increases the key count by 5.

			Destroy(other.gameObject);//Destroys the object.

		}
		if (other.gameObject.CompareTag("Key"))//If the colliding object is a "Key".
		{
			audioSource.PlayOneShot(key, 1.0F);//Plays the Key sound effect.

			Instantiate(keyEffect, transform.position, transform.rotation);//Creates an instance of the key particle effect at the objects position.

			empty.GetComponent<variableHolder>().iKeys++;//Increases the key count by 1.

			Destroy(other.gameObject);//Destroys the object.
		}
		if (other.gameObject.CompareTag("Charge Candy"))//If the colliding object is the "Charge Candy".
		{
			Instantiate(foodEffect, transform.position, transform.rotation);//Creates an instance of the foodEffect particle effect at the objects position.

			audioSource.PlayOneShot(chargeCandy, 1.0F);//Plays the chargeCandy effect.

			empty.GetComponent<variableHolder>().bChargeCandy = true;//Sets the ChargeCandy boolean to true.

			Destroy(other.gameObject);//Destroys the object.
		}
	}

	#endregion

}





