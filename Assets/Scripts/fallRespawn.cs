﻿//\===========================================================================================
//\ File: fallRespawn.cs
//\ Author: Morgan James
//\ Date Created: 21/02/2017
//\ Brief: : Attached to the floor so when the player lands on after a period re spawn at the link position.
//\===========================================================================================

using UnityEngine;

public class fallRespawn : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public GameObject spawn;//The game object that the player will be moved to if it hits this object.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			other.transform.position = spawn.transform.position;//Moves the player to the spawn points transform.
			other.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);//Zero's the players velocity.
		}
	}

	#endregion

}
