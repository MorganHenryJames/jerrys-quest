﻿//\===========================================================================================
//\ File: bossCollider.cs
//\ Author: Morgan James
//\ Date Created: 13/05/2017
//\ Brief: Allows for the object it is attached to, to be hit 3 times by the player before destroying its parent.
//\ Also moves the player to two different way-points and plays particle effects.
//\===========================================================================================

using UnityEngine;

public class bossCollider : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private int iHitPoints = 2;//The "hit points" of the object

	public ParticleSystem deathEffect;//The particle effect that will play when all hit points are gone.

	public ParticleSystem hitEffect;//The particle effect that will play on each hit point loss.

	public Transform waypoint1;//The first point the player gets moved to.

	public Transform waypoint2;//The second point the players gets moved to.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			if (iHitPoints == 2)//If the current hit points equal two.
			{
				Instantiate(hitEffect, other.transform.position, other.transform.rotation);//Creates an instance of the hit particle effect at the players position.

				other.transform.position = waypoint1.position;//Move the player to the first way point.

				other.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);//Zero's the players velocity.

				iHitPoints--;//Removes a hit point from the object.
			}
			else if (iHitPoints == 1)//If the hit points are equal to one.
			{
				Instantiate(hitEffect, other.transform.position, other.transform.rotation);//Creates an instance of the hit particle effect at the players position.

				other.transform.position = waypoint2.position;//Move the player to the second way point.

				other.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);//Zero's the players velocity.

				iHitPoints--;//Removes a hit point from the object.
			}
			else if (iHitPoints == 0)//If the hit points are equal to zero.
			{
				Instantiate(deathEffect, transform.position, transform.rotation);//Creates an instance of the death particle effect at the players position.

				Destroy(transform.parent.gameObject);//Destroys the parent of the object.
			}
		}
	}

	#endregion

}