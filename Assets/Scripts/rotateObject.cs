﻿//\===========================================================================================
//\ File: rotateObject.cs
//\ Author: Morgan James
//\ Date Created: 21/02/2017
//\ Brief: Attached to the pickUp's to make them spin and float.
//\===========================================================================================

using UnityEngine;

public class rotateObject : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public float fDegreesPerSecond = 15.0f;//How much the object rotates.

	public float fAmplitude = 0.1f;//How high and low the object floats.

	public float fFrequency = 0.1f;//How fast the object floats.

	Vector3 v3PosOffset = new Vector3();//Contains where the object is.

	Vector3 v3TempPos = new Vector3();//Contains where the object is going to be.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		v3PosOffset = transform.position;//Store the starting position & rotation of the object
	}

	void Update()
	{
		transform.Rotate(new Vector3(0f, Time.deltaTime * fDegreesPerSecond, 0f), Space.World);//Spin object around Y-Axis.

		v3TempPos = v3PosOffset;//Creates a temporary position equal to the position of the object.

		v3TempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * fFrequency) * fAmplitude;//Float up/down with a Sin().

		transform.position = v3TempPos;//Set the position of the object to the new position.
	}

	#endregion

}
