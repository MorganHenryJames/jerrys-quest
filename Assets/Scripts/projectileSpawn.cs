﻿//\===========================================================================================
//\ File: projectileSpawn
//\ Author: Morgan James
//\ Date Created: 14/05/2017
//\ Brief: Moves the object to be at the same height as the player so that projectiles can shoot stright at the player.
//\===========================================================================================

using UnityEngine;

public class projectileSpawn : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private GameObject Player;//A game object equal to the player.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		Player = GameObject.Find("Player");//Sets the game object to be equal to the player.
	}

	void Update()
	{
		transform.position = new Vector3(transform.position.x, Player.transform.position.y, transform.position.z);//Sets the position of the object to be the same height as the player.
	}

	#endregion
}
