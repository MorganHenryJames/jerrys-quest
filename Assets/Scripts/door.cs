﻿//\===========================================================================================
//\ File: door.cs
//\ Author: Morgan James
//\ Date Created: 09/05/2017
//\ Brief: Allows the object this script is attached to raise when a player with the correct 
//\ amount of keys enters its objects region.
//\===========================================================================================

using UnityEngine;
using UnityEngine.UI;

public class door : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public int ikeysNeeded = 0;//The amount of keys needed to raise the door.

	public Text lockedText;//The UI element that comes up if the player tries entering the door without enough keys.

	public float fDoorRaiseHeight = 3.0f;//How high the object is raised.

	public float fRaiseSpeed = 5.0f;//How fast the object is raised.

	private int iKeys = 0;//How many keys the player has.

	private bool bMoving = false;//True if the object is moving.

	private float fMaxHeight = 0.0f;//The Max hight the object can be raised

	private float fCurrentHeight = 0.0f;//The current hight of the object.

	private GameObject empty;//A game object that will be equal to the permanent variables object.

	public Material red;//The color the object will be if player cannot open the door.

	public Material blue;//The color the object will be if the player can open the door.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		fMaxHeight = transform.position.y + fDoorRaiseHeight;//Sets the max height to be equal to the current door height plus the desired door raise height.

		empty = GameObject.Find("Permanent Variables");//Sets the empty game object equal to the game object that contains the variables that wont be destroyed on scene change.
	}

	void FixedUpdate()
	{
		fCurrentHeight = transform.position.y;//Sets the current height equal to the y of the objects transform.

		if (bMoving == true && fCurrentHeight < fMaxHeight)//If the door is moving and the current height is less that the maximum height execute the following code.
		{
			transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), fRaiseSpeed * Time.deltaTime);//Change the position of the object.
		}

		iKeys = empty.GetComponent<variableHolder>().iKeys;//Sets iKeys to be equal to the amount of keys the player has collected.

		if (ikeysNeeded > iKeys || ikeysNeeded < iKeys)//If the amount of keys collected isn't the same as the amount of keys needed execute the following code.
		{
			GetComponent<Renderer>().material = red;//Set the color of the object to the red material.
		}
		else if (ikeysNeeded == iKeys)//If the amount of keys collected are the same as the keys needed then execute the following code.
		{
			GetComponent<Renderer>().material = blue;//Set the color of the object to the blue material.
		}
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			if (ikeysNeeded > iKeys || ikeysNeeded < iKeys)//If the amount of keys collected by the play is not the same as the keys needed then execute the following code.
			{
				lockedText.text = "It's locked!";//Set the UI text of locked text to be equal to "It's Locked!";
			}
			else if (ikeysNeeded == iKeys)//If the amount of keys collected by the player is the same as the keys needed execute the following code.
			{
				bMoving = true;//Sets the bMoving boolean to true.
			}
		}
	}

	void OnTriggerExit(Collider other)//When an object leaves the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			lockedText.text = "";//Set the UI text of locked text to be equal to "";
		}
	}

	#endregion
}
