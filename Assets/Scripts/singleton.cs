﻿//\===========================================================================================
//\ File: singleton.cs
//\ Author: Morgan James
//\ Date Created: 10/05/2017
//\ Brief: Stops multiple instances of the same class.
//\===========================================================================================

using UnityEngine;

public class singleton : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	private static singleton _instance;//The instance that is currently active.

	public static singleton Instance { get { return _instance; } }//Gets the current instance.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	private void Awake()
	{
		if (_instance != null && _instance != this)//If there is an instance of this object and the instance is not this one execute the following code.
		{
			Destroy(this.gameObject);//Destroy this gameObject.
		}
		else//If there is no instance of this object  or the instance is this game object
		{
			_instance = this;//Set the instance to be equal to this game object
		}
	}

	#endregion

}
