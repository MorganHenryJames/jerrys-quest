﻿//\===========================================================================================
//\ File: characterHolder.cs
//\ Author: Morgan James
//\ Date Created: 26/04/2017
//\ Brief: Parents the player to the object when they collide and sets the parent to null when the player leaves.
//\===========================================================================================

using UnityEngine;

public class characterHolder : MonoBehaviour
{
	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			other.transform.parent = gameObject.transform;//Parents the player to the object.
		}
	}
	void OnTriggerExit(Collider other)//When an object leaves the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			other.transform.parent = null;//Sets the parent to null.
		}
	}

	#endregion

}
