﻿//\===========================================================================================
//\ File: spiderGloveCollider.cs
//\ Author: Morgan James
//\ Date Created: 13/05/2017
//\ Brief: Attached to the hit box for the spider's gloves. Knocks the player back when hit.
//\===========================================================================================

using UnityEngine;

public class spiderGloveCollider : MonoBehaviour
{
	//\===========================================================================================
	//\ Variables
	//\===========================================================================================

	#region Variables

	public float intensity = 30.0f;//How much force to apply to the player on hit.

	public ParticleSystem hitEffect;//The particle effect that will be created on hit.

	public AudioClip[] hits;//The audio clips that will play when the player gets hit by the gloves.

	private AudioSource audioSource;//The audio source from whic hthe sounds will play.

	#endregion

	//\===========================================================================================
	//\ Unity Methods
	//\===========================================================================================

	#region Unity Methods

	void Start()
	{
		audioSource = GetComponent<AudioSource>();//Sets the audio source equal to the one attached to the object.
	}

	void OnTriggerEnter(Collider other)//When another object collides with the object the following code is executed.
	{
		if (other.tag == "Player")//If the colliding object is the "Player".
		{
			int random = Random.Range(0, hits.Length);//Sets an integer to be equal to a random number between 0 and the array size of hits.

			audioSource.PlayOneShot(hits[random]);//Plays a random hit sound from the hits array.

			Instantiate(hitEffect, other.transform.position, other.transform.rotation);//Creates the hit particle effect at the players position.

			other.GetComponent<Rigidbody>().velocity = transform.forward * intensity;//Adds force to the player in the direction forward direction of the gloves multiplied by the intensity.
		}
	}

	#endregion

}
